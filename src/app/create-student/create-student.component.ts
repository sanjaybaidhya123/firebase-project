import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { StudentService } from '../student.service';

@Component({
  selector: 'app-create-student',
  templateUrl: './create-student.component.html',
  styleUrls: ['./create-student.component.css']
})
export class CreateStudentComponent {
  studentForm!: FormGroup;
  constructor(
    public formBuilder: FormBuilder,
    public router: Router,
    public studentService: StudentService
  ) {
    this.studentForm = this.formBuilder.group({
      name: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required]),
      student_course: new FormControl('', [Validators.required]),
      fees: new FormControl('', [Validators.required]),
    });
  }

  submit() {
    this.studentService.createStudent(this.studentForm.value);
    this.router.navigate(['list']);
  }

}
