import { Component, OnInit } from '@angular/core';
import { StudentService } from '../student.service';
import { Student } from '../student.model';

@Component({
  selector: 'app-list-student',
  templateUrl: './list-student.component.html',
  styleUrls: ['./list-student.component.css']
})
export class ListStudentComponent implements OnInit {
  studentList: Student[] = [];
  constructor(
    private studentService: StudentService
  ) { }

  ngOnInit(): void {
    this.studentService.getStudentList()
      .subscribe(
        (res: any) => {
          this.studentList = res.map((e: any) => {
            return {
              id: e.payload.doc.id,
              ...e.payload.doc.data() as {}
            } as Student
          }
          )
        }
      )
  }

  remove(student: any) {
    if (confirm('Are yor sure to delete ' + student.name)) {
      this.studentService.deleteStudent(student);
    }
  }

}
