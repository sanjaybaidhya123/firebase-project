import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { StudentService } from '../student.service';

@Component({
  selector: 'app-edit-student',
  templateUrl: './edit-student.component.html',
  styleUrls: ['./edit-student.component.css']
})
export class EditStudentComponent implements OnInit {
  studentForm!: FormGroup;
  constructor(
    public formBuilder: FormBuilder,
    public router: Router,
    public studentService: StudentService,
    public route: ActivatedRoute
  ) {
    this.studentForm = this.formBuilder.group({
      name: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required]),
      student_course: new FormControl('', [Validators.required]),
      fees: new FormControl('', [Validators.required]),
    });
  }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');

    this.studentService.getStudent(id)
    .subscribe(
      (res: any) =>{
        this.studentForm = this.formBuilder.group({
          name: new FormControl(res.name, [Validators.required]),
          email: new FormControl(res.email, [Validators.required]),
          student_course: new FormControl(res.student_course, [Validators.required]),
          fees: new FormControl(res.fees, [Validators.required]),
        });
      }
    )
  }

  submit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.studentService.updateStudent(this.studentForm.value, id);
    this.router.navigate(['list']);
  }

}
