import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { StudentService } from '../student.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  studentForm!: FormGroup;
  isSignIn = false;
  constructor(
    public formBuilder: FormBuilder,
    public studentService: StudentService,
    public router: Router
  ) {
    this.studentForm = this.formBuilder.group({
      email: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    });
  }

  ngOnInit(): void {
    if(localStorage.getItem('user') != null){
      this.isSignIn = true;
    }
    else {
      this.isSignIn = false;
    }
  }

  async signup(email: any, password: any) {
    await this.studentService.signUp(email, password)
    if(this.studentService.isLoggedIn)
    this.isSignIn = true;
  }

  async submit() {
    await this.studentService.signIn(this.studentForm.value.email, this.studentForm.value.password)
    if(this.studentService.isLoggedIn)
    {
      this.isSignIn = true;
      this.studentService.isLoggedIn = true;
      this.router.navigate(['list']);
    }

  }
}
