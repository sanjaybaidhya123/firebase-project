import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Student } from './student.model';
import { Observable, map } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/compat/auth';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  constructor(
    private angularFireStore: AngularFirestore,
    private auth: AngularFireAuth
  ) { }

  getStudent(id: any) {
    return this.angularFireStore
      .collection('student-collection')
      .doc(id)
      .valueChanges()
  }

  getStudentList() {
    return this.angularFireStore
      .collection('student-collection')
      .snapshotChanges();
  }

  createStudent(student: Student) {
    return new Promise<any>((resolve: any, reject: any) => {
      this.angularFireStore
        .collection('student-collection')
        .add(student)
        .then((response: any) => {
          console.log(response);
        },
          error => reject(error)
        );
    });
  }

  deleteStudent(student: any) {
    return this.angularFireStore
      .collection('student-collection')
      .doc(student.id)
      .delete();
  }


  updateStudent(student: Student, id: any) {
    return this.angularFireStore
      .collection('student-collection')
      .doc(id)
      .update({
        name: student.name,
        email: student.email,
        student_course: student.student_course,
        fees: student.fees
      });
  }

  getUserData(userId: string): Observable<any> {
    return this.angularFireStore.collection('users').doc(userId).valueChanges();
  }

  updateUserData(userId: string, data: any): Promise<void> {
    return this.angularFireStore.collection('users').doc(userId).set(data, { merge: true });
  }

  isAuthenticated(): Observable<boolean> {
    return this.auth.authState.pipe(map(user => user !== null));
  }

  getStudentByEmail(email: any) {
    return this.angularFireStore
      .collection('student-collection')
      .doc(email)
      .valueChanges()
  }

  isLoggedIn = false;

  async signIn(email: any, password: any){
    await this.auth.signInWithEmailAndPassword(email, password)
    .then(
      (res: any) =>{
        this.isLoggedIn = true;
        localStorage.setItem('user', JSON.stringify(res.user));

      }
    )
  }

  async signUp(email: any, password: any){
    await this.auth.signInWithEmailAndPassword(email, password)
    .then(
      (res: any) =>{
        this.isLoggedIn = true;
        localStorage.setItem('user', JSON.stringify(res.user));
        
      }
    )
  }

  logout(){
    this.auth.signOut();
    localStorage.removeItem('user');
  }

}
