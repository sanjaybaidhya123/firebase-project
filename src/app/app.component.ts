import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StudentService } from './student.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'ang-fire';
  constructor(
    public router: Router,
    public studentService: StudentService
  ){}

  ngOnInit(): void {
    if(localStorage.getItem('user') != null){
      this.studentService.isLoggedIn = true;
    }
    else {
      this.studentService.isLoggedIn = false;
    }
  }

  logout(){
    this.studentService.logout();
    this.router.navigate(['login']);
    this.studentService.isLoggedIn = false;
  }
}
